﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PhoneBook.DAL
{
    public class DbManager : IDisposable
    {
        private readonly IDbConnection Conn;

        public DbManager(IDbConnection dbConnection)
        {
            Conn = dbConnection;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DbManager() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


        public UserEntity GetUserByName(string username)
        {
            var sb = new StringBuilder();
            sb.Append(@"Select * From Users Where UserName = @p1;");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", username);
            var user = Conn.QueryFirstOrDefault<UserEntity>(sql, SQLParamet);
            return user;
        }

        public UserEntity Register(string username, string hashpass)
        {
            var sb = new StringBuilder();
            sb.Append(@"INSERT INTO Users (UserName, PasswordHash) VALUES (@p1, @p2);
            Select * from Users Where UserName = @p1 and PasswordHash = @p2");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", username);
            SQLParamet.Add("@p2", hashpass);

            var user = Conn.QueryFirstOrDefault<UserEntity>(sql, SQLParamet);
            return user;
        }

        public List<UserContactsData> GetUserContacts(Guid userId)
        {
            var sb = new StringBuilder();
            sb.Append(@"SELECT
	            c.Id as ContactId,
	            c.ContactName,
	            cp.Phone,
	            cp.Id as PhoneId
            FROM
	            Contacts c
	            LEFT JOIN ContactsPhones cp ON cp.ContactId = c.Id 
            WHERE
	            UserId = @p1
            ORDER BY c.ContactName, c.Id, cp.CreateDate DESC");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", userId);

            var contacts = Conn.Query<UserContactsData>(sql, SQLParamet).ToList();
            return contacts;
        }

        public bool ContactExist(string name, Guid userId)
        {
            var sb = new StringBuilder();
            sb.Append(@"SELECT Id 
            FROM
	            Contacts 
            WHERE
	            ContactName = @p1 AND UserId = @p2");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", name);
            SQLParamet.Add("@p2", userId);

            var exist = Conn.QueryFirstOrDefault<Guid?>(sql, SQLParamet);
            if (exist == null)
                return false;
            return true;
        }

        public bool PhoneExist(string phone)
        {
            var sb = new StringBuilder();
            sb.Append(@"SELECT 1 
            FROM
	            ContactsPhones 
            WHERE
	            Phone = @p1");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", phone);
            var exist = Conn.QueryFirstOrDefault<bool>(sql, SQLParamet);
            return exist;
        }

        public List<UserContactsData> AddNewContactWithPhone(Guid userId, string contactName, List<string> phones)
        {
            var sb = new StringBuilder();
            var SQLParamet = new DynamicParameters();
            sb.Append(@"DECLARE @MyTableVar TABLE ( [testID] [uniqueidentifier] );
            INSERT INTO Contacts ( ContactName, UserId ) 
            OUTPUT Inserted.Id INTO @MyTableVar
            VALUES ( @p2, @p1 );
    
            DECLARE @MyTableVar2 TABLE ( [ID] [uniqueidentifier] );
            INSERT INTO ContactsPhones ( Phone, ContactId )
            OUTPUT Inserted.Id INTO @MyTableVar2
            VALUES ");
            var alreadyBe = false;
            SQLParamet.Add("@p1", userId);
            SQLParamet.Add("@p2", contactName);
            SQLParamet.Add("@p3", phones);
            int index = 4;
            foreach (var phone in phones)
            {
                if (alreadyBe)
                {
                    sb.Append(@", ");
                }
                sb.Append($@"( @p{index}, ( SELECT TOP 1 testId FROM @MyTableVar ))");
                SQLParamet.Add($"@p{index}", phone);
                index++;
                alreadyBe = true;
            }

            sb.Append(@"
            SELECT
	            c.Id as ContactId,
	            c.ContactName,
	            cp.Phone,
	            cp.Id as PhoneId
            FROM
	            Contacts c
	            LEFT JOIN ContactsPhones cp ON cp.ContactId = c.Id 
            WHERE
	            UserId = @p1 And cp.Id in (SELECT * FROM @MyTableVar2)
            ORDER BY c.ContactName, c.Id, cp.CreateDate DESC");
            var sql = sb.ToString();
            var contacts = Conn.Query<UserContactsData>(sql, SQLParamet).ToList();
            return contacts;
        }

        public void DeleteContact(Guid ContactId)
        {
            var sb = new StringBuilder();
            sb.Append(@"DELETE ContactsPhones 
            WHERE
	            ContactId = @p1;
            DELETE Contacts 
            WHERE
	            Id = @p1;");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", ContactId);
            Conn.Query(sql, SQLParamet);
        }

        public bool ContactExistById(Guid ContactId)
        {
            var sb = new StringBuilder();
            sb.Append(@"SELECT 1 From Contacts where Id = @p1");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", ContactId);
            var exist = Conn.QueryFirstOrDefault<bool>(sql, SQLParamet);
            return exist;
        }


        public bool ContactAlreadyHasThisNumber(Guid userGuid, Guid contactId, string phone)
        {
            var sb = new StringBuilder();
            sb.Append(@"SELECT 1 
            FROM
	            Users u
	            LEFT JOIN Contacts c ON c.UserId = u.Id
	            LEFT JOIN ContactsPhones cp ON cp.ContactId = c.Id 
            WHERE
	            u.Id = @p1 
	            AND c.Id = @p2
	            AND cp.Phone = @p3");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", userGuid);
            SQLParamet.Add("@p2", contactId);
            SQLParamet.Add("@p3", phone);
            var exist = Conn.QueryFirstOrDefault<bool>(sql, SQLParamet);
            return exist;
        }


        public Guid AddPhoneToContact(Guid contactId, string phone)
        {
            var sb = new StringBuilder();
            sb.Append(@"INSERT INTO ContactsPhones ( Phone, ContactId )
            OUTPUT INSERTED.ID
            VALUES
	            ( @p2, @p1 );");
            var sql = sb.ToString();
            var SQLParamet = new DynamicParameters();
            SQLParamet.Add("@p1", contactId);
            SQLParamet.Add("@p2", phone);
            var result = Conn.QueryFirstOrDefault<Guid>(sql, SQLParamet);
            return result;
        }



    }
}
