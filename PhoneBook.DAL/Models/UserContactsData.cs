﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DAL.Models
{
    public class UserContactsData
    {
        public Guid ContactId { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public Guid? PhoneId { get; set; }
    }
}
