﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DAL.Models
{
    public class UserEntity
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string PasswordHash { get; set; }
    }
}
