﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneBook.Controllers
{
    public partial class AuthController
    {
        public class LoginRequest
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }

        public class LoginResponse : ResponseBase
        {

        }

        public class RegisterRequest
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }

        public class InitialResponse : ResponseBase
        {
            public string Lang { get; set; }
            public string LangJsonPath { get; set; }

            public bool IsAuthorized { get; set; }
        }

        public class SetLanguageRequest
        {
            public string Culture { get; set; }
        }
    }
}
