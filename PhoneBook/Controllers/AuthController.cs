﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.DAL;
using PhoneBook.DAL.Models;

namespace PhoneBook.Controllers
{
    [Route("api/[controller]/[action]")]
    [AllowAnonymous]
    public partial class AuthController : ControllerBase
    {

        const string DefaultLng = "en-US";
        public AuthController(IComponentContext componentContext) : base(componentContext)
        {

        }

        [HttpPost]
        public InitialResponse Initial()
        {
            InitialResponse resp = new InitialResponse();
            var rqf = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            var ci = rqf.RequestCulture.Culture;
            resp.Lang = ci.Name;
            resp.LangJsonPath = $"lang_{ci.Name.Replace('-', '_')}.json";
            resp.IsAuthorized = IsAuthorized;
            return resp;
        }

        [HttpPost]
        public async Task<LoginResponse> Login([FromBody]LoginRequest req)
        {
            var db = Resolve<DbManager>();
            var user = db.GetUserByName(req.UserName);
            LoginResponse res;
            if (user == null)
            {
                return res = new LoginResponse()
                {
                    Error = "Login Error"
                };
            }

            bool isPasswordOk = false;
            var hasher = new PasswordHasher<UserEntity>();
            var passwordHash = hasher.HashPassword(null, req.Password);
            var verifyPasswordResult = hasher.VerifyHashedPassword(null, user.PasswordHash, req.Password);
            isPasswordOk = verifyPasswordResult == PasswordVerificationResult.Success;

            if (!isPasswordOk)
            {
                return res = new LoginResponse()
                {
                    Error = "Password Error"
                };
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim("UserId", user.Id.ToString())
            };

            var claimsIdentity = new ClaimsIdentity(
                claims,
                CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignInAsync(
               CookieAuthenticationDefaults.AuthenticationScheme,
               new ClaimsPrincipal(claimsIdentity));

            res = new LoginResponse();
            return res;
        }

        
        [HttpPost]
        public IActionResult Register([FromBody]RegisterRequest req)
        {
            string HashPass = string.Empty;
            HashPass = Hash(req.Password);
            var db = Resolve<DbManager>();

            var user = db.Register(req.UserName, HashPass);

            return Ok();
        }

        public string Hash(string Password)
        {
            var hasher = new PasswordHasher<UserEntity>();
            return hasher.HashPassword(null, Password);
        }

        [HttpPost]
        public IActionResult SetLanguage([FromBody]SetLanguageRequest req)
        {
            if (req.Culture == null)
            {
                req.Culture = DefaultLng;
            }
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(req.Culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return Ok();
        }
    }
}