﻿using PhoneBook.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneBook.Controllers
{
    public partial class ContactController
    {
        public class GetContactsResponse : ResponseBase
        {
            public string UserName { get; set; }
            public List<UserContactWithPhoneArray> Contacts { get; set; }
        }

        public class UserContactWithPhoneArray
        {
            public Guid? ContactId { get; set; }
            public string ContactName { get; set; }
            public List<PhoneEntity> Phones { get; set; }
        }

        public class PhoneEntity
        {
            public Guid? Id { get; set; }
            public string Phone { get; set; }
        }

        public class AddContactRequest
        {
            public string Name { get; set; }
            public List<string> Phones { get; set; }
        }
        public class AddContactResponse : ResponseBase
        {
            public UserContactWithPhoneArray Contact { get; set; }
        }

        public class DeleteContactRequest
        {
            public Guid? ContactId { get; set; }
        }
        public class DeleteContactResponse : ResponseBase
        {

        }
        public class EditContactRequest
        {
            public UserContactWithPhoneArray Contact { get; set; }
        }
        public class EditContactResponse : ResponseBase
        {
            public UserContactWithPhoneArray Contact { get; set; }
        }

        public class AddPhoneRequest
        {
            public Guid? ContactId { get; set; }
            public string ContactPhone { get; set; }
        }
        public class AddPhoneResponse : ResponseBase
        {
            public Guid AddedPhoneId { get; set; }
        }



    }
}
