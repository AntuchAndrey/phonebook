﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.DAL;

namespace PhoneBook.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public partial class ContactController : ControllerBase
    {
        public ContactController(IComponentContext componentContext) : base(componentContext)
        {

        }


        [HttpPost]
        public GetContactsResponse GetContacts()
        {
            var db = Resolve<DbManager>();
            var userClaimsValue = User.Claims.First(x => x.Type == "UserId").Value;
            var UserGuid = Guid.Parse(userClaimsValue);
            var contacts = db.GetUserContacts(UserGuid);
            List<UserContactWithPhoneArray> contactsWithPhonesList = new List<UserContactWithPhoneArray>();
            for (int i = 0; i < contacts.Count; i++)
            {
                var ContactExist = contactsWithPhonesList.FirstOrDefault(x => x.ContactId == contacts[i].ContactId);

                if (ContactExist == null)
                {
                    UserContactWithPhoneArray cont = new UserContactWithPhoneArray
                    {
                        ContactId = contacts[i].ContactId,
                        ContactName = contacts[i].ContactName,
                        Phones = new List<PhoneEntity>()
                    };
                    if (contacts[i].PhoneId != null)
                    {
                        var phone = new PhoneEntity()
                        {
                            Id = contacts[i].PhoneId,
                            Phone = contacts[i].Phone
                        };
                        cont.Phones.Add(phone);
                    }
                    contactsWithPhonesList.Add(cont);
                }
                else
                {
                    ContactExist.Phones.Add(new PhoneEntity()
                    {
                        Id = contacts[i].PhoneId.Value,
                        Phone = contacts[i].Phone
                    });
                }

            }

            GetContactsResponse resp = new GetContactsResponse()
            {
                UserName = User.Identity.Name,
                Contacts = contactsWithPhonesList
            };
            return resp;
        }

        [HttpPost]
        public AddContactResponse AddContact([FromBody]AddContactRequest req)
        {
            var userClaimsValue = User.Claims.First(x => x.Type == "UserId").Value;
            var UserGuid = Guid.Parse(userClaimsValue);

            //var user2 = User.Claims.First(x => x.Type == "UserId");
            AddContactResponse resp = new AddContactResponse();
            var db = Resolve<DbManager>();
            //var user = db.GetUserByName(User.Identity.Name);
            if (req == null)
            {
                resp.Error = "Request Error";
                return resp;
            }
            if (string.IsNullOrWhiteSpace(req.Name))
            {
                resp.Error = "Name is empty";
                return resp;
            }
            else
            {
                var exist = db.ContactExist(req.Name, UserGuid);
                if (exist)
                {
                    resp.Error = "User already has this contact";
                    return resp;
                }
            }

            if (req.Phones?.Count == 0)
            {
                resp.Error = "Phones is empty";
                return resp;
            }
            else
            {
                foreach (var phone in req.Phones)
                {
                    var exist = db.PhoneExist(phone);
                    if (exist)
                    {
                        resp.Error = "This phone already exist";
                        return resp;
                    }
                }
            }

            var contacts = db.AddNewContactWithPhone(UserGuid, req.Name, req.Phones);
            UserContactWithPhoneArray contact = new UserContactWithPhoneArray()
            {
                ContactId = contacts[0].ContactId,
                ContactName = contacts[0].ContactName,
                Phones = new List<PhoneEntity>()
            };
            foreach (var cont in contacts)
            {
                contact.Phones.Add(
                    new PhoneEntity
                    {
                        Id = cont.PhoneId,
                        Phone = cont.Phone
                    }
                );
            }

            resp.Contact = contact;

            return resp;
        }

        public DeleteContactResponse DeleteContact([FromBody]DeleteContactRequest req)
        {
            DeleteContactResponse resp = new DeleteContactResponse();
            if (req == null)
            {
                resp.Error = "Request Error";
                return resp;
            }

            if (req.ContactId == null)
            {
                resp.Error = "ContactId Empty";
                return resp;
            }
            var db = Resolve<DbManager>();

            var exist = db.ContactExistById(req.ContactId.Value);
            if (!exist)
            {
                resp.Error = "Contact not exist";
                return resp;
            }
            db.DeleteContact(req.ContactId.Value);
            return resp;
        }

        public EditContactResponse EditContact([FromBody]EditContactRequest req)
        {
            EditContactResponse resp = new EditContactResponse();
            if (req == null)
            {
                resp.Error = "Request Error";
                return resp;
            }

            if (req.Contact == null)
            {
                resp.Error = "Contact Empty";
                return resp;
            }

            if (req.Contact.ContactId == null)
            {
                resp.Error = "ContactId Empty";
                return resp;
            }
            var db = Resolve<DbManager>();
            var exist = db.ContactExistById(req.Contact.ContactId.Value);
            if (!exist)
            {
                resp.Error = "Contact not exist";
                return resp;
            }

            db.DeleteContact(req.Contact.ContactId.Value);

            var userClaimsValue = User.Claims.First(x => x.Type == "UserId").Value;
            var UserGuid = Guid.Parse(userClaimsValue);
            List<string> phones = req.Contact.Phones.Select(x => x.Phone).ToList();
            var contacts = db.AddNewContactWithPhone(UserGuid, req.Contact.ContactName, phones);
            UserContactWithPhoneArray contact = new UserContactWithPhoneArray()
            {
                ContactId = contacts[0].ContactId,
                ContactName = contacts[0].ContactName,
                Phones = new List<PhoneEntity>()
            };
            foreach (var cont in contacts)
            {
                contact.Phones.Add(
                    new PhoneEntity
                    {
                        Id = cont.PhoneId,
                        Phone = cont.Phone
                    }
                );
            }

            resp.Contact = contact;

            return resp;
        }
        public AddPhoneResponse AddPhone([FromBody]AddPhoneRequest req)
        {
            AddPhoneResponse resp = new AddPhoneResponse();
            if (req == null)
            {
                resp.Error = "Request Error";
                return resp;
            }

            if (req.ContactId == null)
            {
                resp.Error = "Contact Empty";
                return resp;
            }
            if (string.IsNullOrWhiteSpace(req.ContactPhone))
            {
                resp.Error = "Phone Empty";
                return resp;
            }

            var db = Resolve<DbManager>();
            var exist = db.ContactExistById(req.ContactId.Value);
            if (!exist)
            {
                resp.Error = "Contact not exist";
                return resp;
            }

            var userClaimsValue = User.Claims.First(x => x.Type == "UserId").Value;
            var UserGuid = Guid.Parse(userClaimsValue);

            exist = db.ContactAlreadyHasThisNumber(UserGuid, req.ContactId.Value, req.ContactPhone);
            if (exist)
            {
                resp.Error = "Contact Already Has This Number";
                return resp;
            }

            var id = db.AddPhoneToContact(req.ContactId.Value, req.ContactPhone);
            resp.AddedPhoneId = id;
            return resp;
        }
    }
}