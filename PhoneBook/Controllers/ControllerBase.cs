﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Mvc;

namespace PhoneBook.Controllers
{
    public abstract class ControllerBase : Controller
    {
        protected readonly IComponentContext ComponentContext;
        protected bool IsAuthorized => User != null && User.Identity.IsAuthenticated;
        public ControllerBase(IComponentContext componentContext)
        {
            ComponentContext = componentContext;
        }

        protected T Resolve<T>()
        {
            var result = ComponentContext.Resolve<T>();
            return result;
        }

        protected bool TryResolve<T>(out T instance)
        {
            var result = ComponentContext.TryResolve(out instance);
            return result;
        }

        protected T ResolveNamed<T>(string serviceName)
        {
            var result = ComponentContext.ResolveNamed<T>(serviceName);
            return result;
        }

        protected T ResolveKeyed<T>(object key)
        {
            var result = ComponentContext.ResolveKeyed<T>(key);
            return result;
        }
    }
}