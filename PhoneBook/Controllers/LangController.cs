﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PhoneBook.Controllers
{
    [Route("lang_{lang}.json")]
    public class LangController : ControllerBase
    {
        public LangController(ILifetimeScope lifetimeScope) : base(lifetimeScope)
        {
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 86_400)]
        public IActionResult Get(string lang)
        {
            var bytes = (byte[])Properties.Resources.ResourceManager.GetObject($"Localization_{lang}");
            if (bytes == null)
            {
                return NotFound();
            }
            return this.File(bytes, "application/json");
        }
    }
}