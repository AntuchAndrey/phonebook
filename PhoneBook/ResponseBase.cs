﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneBook
{
    public abstract class ResponseBase
    {
        public string Error { get; set; }
    }
}
