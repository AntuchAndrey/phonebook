export const defaultLng = 'he-IL';

export const locales = [
    {
        lng: 'en-US',
        name: 'English',
        dir: 'ltr',
        subtag: 'en',
        image: 'img/usa.svg',
    },
    {
        lng: 'ru-RU',
        name: 'Русский',
        dir: 'ltr',
        subtag: 'ru',
        image: 'img/russia.svg',
    },
];
