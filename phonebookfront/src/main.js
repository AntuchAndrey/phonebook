import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18next from 'i18next';
import $ from 'jquery/dist/jquery.min.js';
import api from '@/services/api.js';
import { locales } from '@/config/i18n';
import VModal from 'vue-js-modal';
import Q from 'q';
import swal from "sweetalert2";
// import 'jquery/dist/jquery.min.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

let LangArray;
let currentLng = '';
init();
async function init() {
  swal.showLoading();
  const delayPromise = Q.delay(200);
  await delayPromise;
  Vue.use(VModal);
  Vue.config.productionTip = false;
  const [err1, response] = await api.Initial();
  store.commit('Authoraize', response.data.IsAuthorized);
  Vue.prototype.$currentLocale = locales.find((d) => d.lng === response.data.Lang);
  currentLng = response.data.Lang;
  const [err2, response2] = await api.GetLangJson(response.data.LangJsonPath);

  LangArray = response2.data;

  createApp(response);
  setDir();
  swal.close();
}
function setDir() {
  const dir = store.state.dirBody;
  const lng = store.state.currentsubtag;
  $(document).ready(() => {
    $('body').attr('dir', dir);
    $('html').attr('lang', lng);
  });
}
function createApp(translation) {
  new Vue({
    router,
    store,
    components: { App },
    render: (h) => h(App),
    template: '<App/>',
    beforeCreate: () => {
      i18next.init({
        lng: currentLng || undefined,
        keySeparator: '>',
        nsSeparator: '|',
        debug: false,
      }, (err, t) => {
        if (err) {
          return;
        }
        t('key');
      });
      store.state.dirBody = Vue.prototype.$currentLocale.dir;
      store.state.currentsubtag = Vue.prototype.$currentLocale.subtag;
      store.state.currentLng = Vue.prototype.$currentLocale.lng;
      store.state.currentLngImg = Vue.prototype.$currentLocale.image;
      i18next.addResourceBundle(String(currentLng), 'translation', translation);
    },
  }).$mount('#app');
}

Vue.mixin({
  data() {
    return {
      IsLandscape: false,
      NameRegex: '^[ A-zА-я\u0590-\u05fe]+$', // white-space symbols, space and Latin/Cyrillic/Hebrew
      LanguageValue: LangArray,
      dirBody: '',
    };
  },
  methods: {
    t(key) {
      const result = this.$data.LanguageValue[key];
      return result ? result : (key);
    },
  },
});
