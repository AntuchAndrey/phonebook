import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Main from './views/Main.vue'
import store from '@/store.js';

Vue.use(Router)

const router = new Router({
  routes: [
      {
          meta: {
              title: 'Login',
              AllowAnonymous: true,
          },
          path: '/Login',
          name: 'Login',
          component: Login,
      },
      {
        meta: {
            title: 'Register',
            AllowAnonymous: true,
        },
        path: '/Register',
        name: 'Register',
        component: Register,
    },
      {
          meta: {
              title: 'Main',
              AllowAnonymous: false,
          },
          path: '/Main',
          name: 'Main',
          component: Main,
      },
  ],
});

router.beforeEach(async (to, from, next) => {
  if (!to.meta.AllowAnonymous && !store.state.isAuthoraized) {
      next({
          path: '/Login',
      });
      return;
  }

  if (to.path === '/') {
      next({
          path: '/Main',
      });
      return;
  }
  next();
});

export default router;

