import axiosLib from 'axios';
import to from 'await-to-js';
const axios = axiosLib.create({
    withCredentials: true,
    baseURL: process.env.VUE_APP_BASE_API_URL,
});
axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
    const custErr = {
        errObj: error,
        customText: '',
    };
    if (error.response) {
        if (error.response.status === 401) {
            custErr.customText = 'Unauthorized. Logged in on another device?';
        }
        else {
            custErr.customText = 'Server error';
        }
    }
    else if (error.request) {
        custErr.customText = 'Network error';
    }
    else {
        custErr.customText = error.message;
    }
    return Promise.reject(custErr);
});
const api = {
    async axiosWrapper(req) {
        const promise = axios(req);
        const [err, axiosResponse] = await to(promise);
        return [err, axiosResponse];
    },
    async Register(username, password) {
        const url = '/api/Auth/Register';
        const reqData = {
            Username: username,
            Password: password,
        };
        const req = {
            method: 'post',
            url,
            data: reqData,
        };
        return await this.axiosWrapper(req);
    },
    async Login(username, password) {
        const url = '/api/Auth/Login';
        const reqData = {
            Username: username,
            Password: password,
        };
        const req = {
            method: 'post',
            url,
            data: reqData,
        };
        return await this.axiosWrapper(req);
    },
    async Initial() {
        const url = '/api/Auth/Initial';
        const req = {
            method: 'post',
            url,
        };
        return await this.axiosWrapper(req);
    },
    async GetLangJson(path) {
        const url = path;
        const req = {
            method: 'post',
            url,
        };
        return await this.axiosWrapper(req);
    },
    async SetLanguage(name) {
        const url = '/api/Auth/SetLanguage';
        const reqData = {
            Culture: name,
        };
        const req = {
            method: 'post',
            url,
            data: reqData,
        };
        return await this.axiosWrapper(req);
    },
    async GetContacts() {
        const url = '/api/Contact/GetContacts';
        const req = {
            method: 'post',
            url,
        };
        return await this.axiosWrapper(req);
    },
    async AddNewContact(contactName, phones) {
        const url = '/api/Contact/AddContact';
        const reqData = {
            Name: contactName,
            Phones: phones
        };
        const req = {
            method: 'post',
            url,
            data: reqData,
        };
        return await this.axiosWrapper(req);
    },
    async DeleteContact(contactId) {
        const url = '/api/Contact/DeleteContact';
        const reqData = {
            ContactId: contactId
        };
        const req = {
            method: 'post',
            url,
            data: reqData,
        };
        return await this.axiosWrapper(req);
    },
    async EditContact(contact){
        const url = '/api/Contact/EditContact';
        const reqData = {
            Contact: contact
        };
        const req = {
            method: 'post',
            url,
            data: reqData,
        };
        return await this.axiosWrapper(req);
    },
    async AddPhone(contactId, phone){
        const url = '/api/Contact/AddPhone';
        const reqData = {
            ContactId: contactId,
            ContactPhone: phone
        };
        const req = {
            method: 'post',
            url,
            data: reqData,
        };
        return await this.axiosWrapper(req);
    }
    
};
export default api;