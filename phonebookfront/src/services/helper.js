import router from '@/router.js';
import _ from 'lodash';
// import ScriptLoader from '@/services/ScriptLoader';
import api from '@/services/api.js';
import swal from 'sweetalert2';

export default {
    // swal: swalLib.mixin({
    //     customClass: 'crm',
    //     background: '#FFFFFF',
    //     allowOutsideClick: false,
    //     onAfterClose: () => {
    //         if (router.currentRoute.query.alert) {
    //             router.back();
    //         }
    //     },
    // }),
    swalShowLoading() {
        this.swal.fire({ text: 'אנא המתן/י' });
        this.swal.showLoading();
    },
    close() {
        this.swal.close();
    },
    copyRoute(route) {
        return {
            name: route.name,
            params: _.clone(route.params),
            query: _.clone(route.query),
            meta: _.clone(route.meta),
        };
    },
    pushQuery(key) {
        const route = this.copyRoute(router.currentRoute);
        route.query = {};
        route.query[key] = 'y';
        router.push(route);
    },
    replaceQuery(key) {
        const route = this.copyRoute(router.currentRoute);
        route.query = {};
        route.query[key] = 'y';
        router.replace(route);
    },
    clearQuery() {
        const route = this.copyRoute(router.currentRoute);
        route.query = {};
        router.push(route);
    },
    setKeyToQuery(key, value) {
        const route = this.copyRoute(router.currentRoute);
        if (value === undefined) {
            value = 'y';
        }
        if (!route.query[key]) {
            route.query[key] = value;
        }
        if (route.query[key] !== router.currentRoute.query[key]) {
            router.push(route);
        }
    },
    removeKeyFromQuery(key, orGoBack) {
        const route = this.copyRoute(router.currentRoute);
        if (route.query[key]) {
            delete route.query[key];
            if (orGoBack) {
                router.back();
            } else {
                router.replace(route);
            }
        }
    },
    getCookie(name) {
        const nameEQ = name + '=';
        const ca = document.cookie.split(';');
        for (let c of ca) {
            while (c.charAt(0) === ' ') { c = c.substring(1, c.length); }
            if (c.indexOf(nameEQ) === 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    },
    setCookie(name, value, days) {
        let expires = '';
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = '; expires=' + date.toUTCString();
        }
        document.cookie = name + '=' + (value || '') + expires + '; path=/';
    },
    async getRefData() {
        const [errRef, responseRef] = await api.GetRefData();
        if (errRef == null) {
            if (responseRef.data.error == null) {
                // const common = responseRef.data.common;
                // const client = responseRef.data.client;
                // if (common) {
                //     ScriptLoader.loadRefData(common, () => {
                //         const data = window;
                //         store.commit('UpdateCommon', data.refdata_common);
                //     });
                // }
                // if (client) {
                //     // ScriptLoader.loadRefData(client, () => {
                //     //     // const data = window;
                //     //     // store.commit('UpdateClientData', data.refdata);
                //     // });
                // }
            }
        }
    },
    async logOut() {
        swal.showLoading();
        const [err, response] = await api.LogOut();
        swal.close();
        if (err == null) {
            if (response.data.error == null) {
                // store.commit('Authoraize', false);
                router.push('Login');
            } else {
                swal.fire({
                    imageUrl: 'img/confirmation.svg',
                    imageWidth: 68,
                    imageHeight: 68,
                    text: response.data.error,
                    confirmButtonColor: '#46d8a3',
                });
            }
        } else {
            swal.fire({
                imageUrl: 'img/confirmation.svg',
                imageWidth: 68,
                imageHeight: 68,
                text: err.errObj,
                confirmButtonColor: '#46d8a3',
            });
        }
    },
};
