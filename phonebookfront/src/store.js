import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isAuthoraized: false,
    currentLng: '',
    currentLngImg: '',
    currentsubtag: '',
  },
  mutations: {
    Authoraize(state, data) {
      state.isAuthoraized = data;
    },
  },
  actions: {
  },
});
